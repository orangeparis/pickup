package vm

import (
	"io/ioutil"
	"log"
	"path/filepath"

	"bitbucket.org/orangeparis/pickup/config"
	"bitbucket.org/orangeparis/pickup/repository"
)

/*
	implements a vm git repository
*/

// GitRepository : a vm git repository
type GitRepository struct {
	repository.BaseGitRepository // a basic git implemention ( Close/Clone ...)
}

// NewGitRepository : create a git repository handler
func NewGitRepository(url string, path string, user string, password string) (git *GitRepository) {
	base := repository.BaseGitRepository{Url: url, Path: path, User: user, Password: password}
	git = &GitRepository{base}
	return git
}

// NewGitRepositoryFromConfig : create a git repository handler from config
func NewGitRepositoryFromConfig() (repository *GitRepository) {
	repository = NewGitRepository(
		config.GetString("repository.url"),
		config.GetString("vars.tmp"),
		config.GetString("repository.user"),
		config.GetString("repository.password"))
	repository.AuthorName = config.GetString("repository.author")
	repository.AuthorEmail = config.GetString("repository.email")

	return repository

}

//
// implements VmRepository interface
//

// Platforms : return a list of vm csv files
func (r *GitRepository) Platforms() (list []string) {
	// find list of vm config <path>/ines-deployment/vm/*.csv
	base := filepath.Join(r.Path, "ines-deployment", "vm")
	// scan csv files
	fileInfo, err := ioutil.ReadDir(base)
	if err != nil {
		return list
	}
	for _, file := range fileInfo {
		name := file.Name()
		list = append(list, name)
	}
	return list
}

// GetPlatform : return the platform objet with this name
func (r *GitRepository) GetPlatform(name string) (data []byte, err error) {
	ptf := filepath.Join(r.Path, "ines-deployment", "vm", name)
	data, err = ioutil.ReadFile(ptf)
	if err != nil {
		log.Printf("cannot find csv file at %s\n", ptf)
		return data, err
	}
	return data, err
}
