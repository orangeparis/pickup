package vm_test

import (
	"fmt"
	"testing"

	"bitbucket.org/orangeparis/pickup/repository/vm"
)

func TestMockRepository(*testing.T) {

	r := vm.NewMockRepository("")
	vm.SetRepository(r)

	vms := vm.Platforms()
	fmt.Println(vms)

	data, _ := vm.GetPlatform("demo")
	fmt.Println(string(data))

}
