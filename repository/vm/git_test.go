package vm_test

import (
	"fmt"
	"log"
	"testing"

	"bitbucket.org/orangeparis/pickup/config"

	//"bitbucket.org/orangeparis/pickup/repository"
	"bitbucket.org/orangeparis/pickup/repository/vm"
)

func TestGitRepository(t *testing.T) {

	config.Load()

	r := vm.NewGitRepositoryFromConfig()
	vm.SetRepository(r)
	defer vm.Close()

	path, err := vm.Clone()
	if err == nil {
		// update the temporary path in config
		config.Set("vars.tmp", path)
	}

	// recreate repository object
	r = vm.NewGitRepositoryFromConfig()
	vm.SetRepository(r)

	pls := vm.Platforms()
	fmt.Println(pls)

	if len(pls) <= 0 {
		log.Printf("cannot find vms")
		t.Fail()
		return
	}
	demo := pls[0]

	data, _ := vm.GetPlatform(demo)
	fmt.Println(string(data))

}

// func TestGitPull(t *testing.T) {

// 	config.Load()

// 	r := repository.NewGitRepositoryFromConfig()
// 	path, err := repository.Clone()
// 	_ = path
// 	if err != nil {
// 		t.Fail()
// 		return
// 	}
// 	// update temporary path
// 	r.Path = path

// 	// set the repository
// 	repository.SetRepository(r)
// 	defer repository.Close()

// 	err = repository.Pull()
// 	if err != nil {
// 		t.Fail()
// 	}

// 	pls := repository.Platforms()
// 	fmt.Println(pls)

// 	ptf, _ := repository.GetPlatform("demo")
// 	fmt.Println(ptf.Name)
// 	fmt.Println(string(ptf.LiveboxCsv))

// }
