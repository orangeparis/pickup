package vm

import (
	"bitbucket.org/orangeparis/pickup/repository"
)

/*

	a vm git repository
	implements
		BaseRepository
		VmRepository


*/

// VmRepository : a git like repository of platform configurations
type VmRepository interface {
	repository.Repository               // Close(),Clone(), ....
	Platforms() []string                // List vm platforms
	GetPlatform(string) ([]byte, error) // get a Platform data

}

// local variable for injection dependency
var impl VmRepository

// SetRepository : setting the repository ( dependecy injection )
func SetRepository(repository VmRepository) {
	impl = repository
}

func Close() {
	impl.Close()
}

func Clone() (path string, err error) {
	return impl.Clone()
}

func Pull() (err error) {
	return impl.Pull()
}

// Platforms : return a list of platform name
func Platforms() (list []string) {
	return impl.Platforms()
}

//GetPlatform : return the platform objet with this name
func GetPlatform(name string) ([]byte, error) {
	return impl.GetPlatform(name)
}
