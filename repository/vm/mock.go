package vm

import "bitbucket.org/orangeparis/pickup/repository"

/*
	implements a mock repository

*/

var vms = []string{"vm1", "vm2"}

var vmCsv = `Force;NameVM;vCenter;vCenterUser;vCenterPass;Cluster;ESX;Template;TemplateNIC;TemplateNbNIC;DefautLanNIC;TemplateUser;TemplatePwd;CPU;RAM;HDD1Datastore;HDD1SuppSize;HDD1SuppDatastore;HDD2Size;HDD2Datastore;HDD3Size;HDD3Datastore;VMIP;VMMask;VMGTW;DNSSEARCH;DNSNAME1;DNSNAME2;LAN1;IP1;Mask1;GTW1;LAN2;IP2;Mask2;GTW2;LAN3;IP3;Mask3;GTW3;Lan4;IP4;Mask4;GTW4;LAN5;IP5;Mask5;GTW5;LAN6;IP6;Mask6;GTW6
1;innapnoi1rad01;qviaso94.creteil.francetelecom.fr;adint/AdminINES;Pime_2018;CLNIA45;innapesxnoi003.creteil.francetelecom.fr;TPL-INES-G03R02C02;eth0;4;AD_SERVEURS_LAB;root;PLATON;2;4;HN420_Datastore_VMDK;;;;;;;10.104.120.94;255.255.255.192;10.104.120.126;;;;BACK_INTERNAL_LAB;;;;PFS1_LAB;;;;STOCKAGE_VM_LAB;;;;;;;;;;;;;;;
1;innapnoi1nev01;qviaso94.creteil.francetelecom.fr;adint/AdminINES;Pime_2018;CLNIA45;innapesxnoi003.creteil.francetelecom.fr;TPL-INES-G03R02C02;eth0;4;AD_SERVEURS_LAB;root;PLATON;2;4;HN420_Datastore_VMDK;;;;;;;10.104.120.95;255.255.255.192;10.104.120.126;;;;BACK_INTERNAL_LAB;;;;PFS1_LAB;;;;STOCKAGE_VM_LAB;;;;;;;;;;;;;;;
1;innapnoi1nev02;qviaso94.creteil.francetelecom.fr;adint/AdminINES;Pime_2018;CLNIA45;innapesxnoi003.creteil.francetelecom.fr;TPL-INES-G03R02C02;eth0;4;AD_SERVEURS_LAB;root;PLATON;2;4;HN420_Datastore_VMDK;;;;;;;10.104.120.96;255.255.255.192;10.104.120.126;;;;BACK_INTERNAL_LAB;;;;PFS1_LAB;;;;STOCKAGE_VM_LAB;;;;;;;;;;;;;;;
1;innapnoi1dep01;qviaso94.creteil.francetelecom.fr;adint/AdminINES;Pime_2018;CLNIA45;innapesxnoi003.creteil.francetelecom.fr;TPL-INES-G03R02C02;eth0;4;AD_SERVEURS_LAB;root;PLATON;4;8;HN420_Datastore_VMDK;20;HN420_Datastore_VMDK;;;;;10.104.120.99;255.255.255.192;10.104.120.126;;;;BACK_INTERNAL_LAB;;;;PFS1_LAB;;;;STOCKAGE_VM_LAB;;;;;;;;;;;;;;;
1;innapnoi1rob01;qviaso94.creteil.francetelecom.fr;adint/AdminINES;Pime_2018;CLNIA45;innapesxnoi003.creteil.francetelecom.fr;TPL-INES-ROB;eth0;6;AD_SERVEURS_LAB;root;PLATON;4;8;HN420_Datastore_VMDK;;;;;;;10.104.120.97;255.255.255.192;10.104.120.126;;;;BACK_INTERNAL_LAB;;;;FRONT_RADIUS_LAB;;;;PFS1_LAB;;;;PFS2_LAB;;;;STOCKAGE_VM_LAB;;;;;;;
1;innapnoi1rob02;qviaso94.creteil.francetelecom.fr;adint/AdminINES;Pime_2018;CLNIA45;innapesxnoi003.creteil.francetelecom.fr;TPL-INES-ROB;eth0;6;AD_SERVEURS_LAB;root;PLATON;4;8;HN420_Datastore_VMDK;;;;;;;10.104.120.98;255.255.255.192;10.104.120.126;;;;BACK_INTERNAL_LAB;;;;FRONT_RADIUS_LAB;;;;PFS1_LAB;;;;PFS2_LAB;;;;STOCKAGE_VM_LAB;;;;;;;
1;innapnoi3dep01;qviaso94.creteil.francetelecom.fr;adint/AdminINES;Pime_2018;CLNIA45;innapesxnoi003.creteil.francetelecom.fr;TPL-INES-G03R02C02;eth0;3;AD_SERVEURS_INT_A;root;PLATON;6;12;HN420_Datastore_VMDK;20;HN420_Datastore_VMDK;;;;;10.97.128.1;255.255.255.192;10.97.129.62;;;;BACK_INTERNAL_INT_A;;;;PFS1_INT_A;;;;;;;;;;;;;;;;;;;
`

// MockRepository : a mock of a vm git repository
type MockRepository struct {
	repository.MockRepository
	Base string // the Base directory
}

// NewMockRepository : create a mock of a git repository
func NewMockRepository(base string) (repo *MockRepository) {
	model := repository.MockRepository{}
	repo = &MockRepository{model, base}
	return repo
}

//
// implements Repository interface
//


// Platforms : return a list of platform name
func (r *MockRepository) Platforms() (list []string) {
	list = []string{"innapnoi1rad01", "innapnoi1nev01"}
	return list
}

// GetPlatform : return the platform objet with this name
func (r *MockRepository) GetPlatform(name string) (vm []byte, err error) {
	vm = []byte("1;innapnoi1rad01;qviaso94.creteil.francetelecom.fr;adint/AdminINES;Pime_2018;CLNIA45;innapesxnoi003.creteil.francetelecom.fr;TPL-INES-G03R02C02;eth0;4;AD_SERVEURS_LAB;root;PLATON;2;4;HN420_Datastore_VMDK;;;;;;;10.104.120.94;255.255.255.192;10.104.120.126;;;;BACK_INTERNAL_LAB;;;;PFS1_LAB;;;;STOCKAGE_VM_LAB;;;;;;;;;;;;;;;")
	return []byte(vmCsv), err
}
