package repository

/*

	a repository of platforms configuration ( liveboxcsv , platform adress ...)
	basicaly a git repository

*/

// Repository : a git like repository of platform configurations
type Repository interface {
	Root() (path string)
	Close()
	Clone() (path string, err error)            // Clone the repository, return the temporary path
	Pull() error                                // update the repository
	Commit(msg string) (hash string, err error) // commit to repository
}

// local variable for injection dependency
var impl Repository

// SetRepository : setting the repository ( dependecy injection )
func SetRepository(repository Repository) {
	impl = repository
}

// Root : return the path where to to find the cloned files
func Root() (path string) {
	return impl.Root()
}

// Close  clear temporary directory
func Close() {
	impl.Close()
}

// Close : Clone the repository into a temporary directory
func Clone() (path string, err error) {
	return impl.Clone()
}

// Pull : git pull
func Pull() (err error) {
	return impl.Pull()
}

// Commit : commit changes to repository
func Commit(msg string) (hash string, err error) {
	return impl.Commit(msg)
}
