package livebox

import (
	"bitbucket.org/orangeparis/pickup/repository"
)

/*

	a livebox git repository
	implements
		BaseRepository
		LiveboxRepository


*/

// LiveboxRepository : a git like repository of platform configurations
type LiveboxRepository interface {
	repository.Repository               // Close(),Clone(), ....
	Platforms() []string                // List platforms
	GetPlatform(string) ([]byte, error) // get a Platform

}

// local variable for injection dependency
var impl LiveboxRepository

// SetRepository : setting the repository ( dependecy injection )
func SetRepository(repository LiveboxRepository) {
	impl = repository
}

// Close  clear temporary directory
func Close() {
	impl.Close()
}

func Clone() (path string, err error) {
	return impl.Clone()
}

func Pull() (err error) {
	return impl.Pull()
}

// Platforms : return a list of platform name
func Platforms() (list []string) {
	return impl.Platforms()
}

//GetPlatform : return the platform objet with this name
func GetPlatform(name string) ([]byte, error) {
	return impl.GetPlatform(name)
}
