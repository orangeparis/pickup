package livebox

import (
	"io/ioutil"
	"log"
	"path/filepath"

	"bitbucket.org/orangeparis/pickup/config"
	"bitbucket.org/orangeparis/pickup/repository"
)

/*
	implements a livebox git repository
*/

// GitRepository : a vm git repository
type GitRepository struct {
	repository.BaseGitRepository // a basic git implementation ( Close/Clone ...)
}

// NewGitRepository : create a git repository handler
func NewGitRepository(url string, path string, user string, password string) (git *GitRepository) {
	base := repository.BaseGitRepository{Url: url, Path: path, User: user, Password: password}
	git = &GitRepository{base}
	return git
}

// NewGitRepositoryFromConfig : create a git repository handler from config
func NewGitRepositoryFromConfig() (repository *GitRepository) {
	repository = NewGitRepository(
		config.GetString("repository.url"),
		config.GetString("vars.tmp"),
		config.GetString("repository.user"),
		config.GetString("repository.password"))
	repository.AuthorName = config.GetString("repository.author")
	repository.AuthorEmail = config.GetString("repository.email")
	return repository

}

// Platforms : return a list of platform directories
func (r *GitRepository) Platforms() (files []string) {

	base := filepath.Join(r.Path, "ines-deployment", "infra")
	files, err := repository.IOReadDir(base)
	if err != nil {
		log.Printf("cannot list platforms: %s", err.Error())
	}
	return files
}

// GetPlatform : return the platform objet with this name
func (r *GitRepository) GetPlatform(name string) (data []byte, err error) {

	//  compute csv filename <root>/ines-deployment/infra/<ptf>/files/livebox.csv
	pfile := filepath.Join(r.Path, "ines-deployment", "infra", name, "files", "livebox.csv")
	data, err = ioutil.ReadFile(pfile)
	if err != nil {
		log.Printf("cannot find livebox csv file at %s\n", pfile)
		return data, err
	}

	return data, err
}
