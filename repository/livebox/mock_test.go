package livebox_test

import (
	"fmt"
	"testing"

	"bitbucket.org/orangeparis/pickup/repository/livebox"
)

func TestMockRepository(*testing.T) {

	r := livebox.NewMockRepository("")
	livebox.SetRepository(r)

	pls := livebox.Platforms()
	fmt.Println(pls)

	ptf, _ := livebox.GetPlatform("demo")
	fmt.Println(ptf.Name)
	fmt.Println(string(ptf.LiveboxCsv))

}
