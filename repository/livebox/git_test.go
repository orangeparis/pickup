package livebox_test

import (
	"fmt"
	"log"
	"testing"

	"bitbucket.org/orangeparis/pickup/config"
	"bitbucket.org/orangeparis/pickup/repository/livebox"
)

var fixtureRepository = "https://bitbucket.org/orangeparis/pickupfixture"

func TestGitRepository(t *testing.T) {

	config.Set("repository.url", fixtureRepository)
	config.Load()

	r := livebox.NewGitRepositoryFromConfig()
	livebox.SetRepository(r)
	defer livebox.Close()

	path, err := livebox.Clone()
	if err != nil {
		log.Printf("Error: %s\n", err.Error())
		t.Fail()
		return
	}
	// update the temporary path in config
	config.Set("vars.tmp", path)

	// recreate repository object
	r = livebox.NewGitRepositoryFromConfig()
	livebox.SetRepository(r)

	pls := livebox.Platforms()
	fmt.Println(pls)

	if len(pls) <= 0 {
		log.Printf("cannot find platforms")
		t.Fail()
		return
	}
	demo := pls[0]

	ptf, _ := livebox.GetPlatform(demo)
	fmt.Println(ptf.Name)
	fmt.Println(string(ptf.LiveboxCsv))

}

// func TestGitPull(t *testing.T) {

// 	config.Load()

// 	r := repository.NewGitRepositoryFromConfig()
// 	path, err := repository.Clone()
// 	_ = path
// 	if err != nil {
// 		t.Fail()
// 		return
// 	}
// 	// update temporary path
// 	r.Path = path

// 	// set the repository
// 	repository.SetRepository(r)
// 	defer repository.Close()

// 	err = repository.Pull()
// 	if err != nil {
// 		t.Fail()
// 	}

// 	pls := repository.Platforms()
// 	fmt.Println(pls)

// 	ptf, _ := repository.GetPlatform("demo")
// 	fmt.Println(ptf.Name)
// 	fmt.Println(string(ptf.LiveboxCsv))

// }
