package repository

import (
	"log"
)

/*
	implements a mock directory
		Close()
		Clone() (path string, err error) // Clone the repository, return the temporary path
		Pull() error                     // update the repository
*/

// MockRepository : implementation of dummy repository
type MockRepository struct {
	Path string // path directory
}

// NewMockRepository : create a base git repository handler
func NewMockRepository(path string) (repository *MockRepository) {
	repository = &MockRepository{Path: path}
	return repository
}

//
// implements Repository interface
//
// Root:  return the root path ( where files where cloned)
func (r *MockRepository) Root() (path string) {
	return r.Path
}

// Close : remove temprary dir
func (r *MockRepository) Close() {
	log.Printf("CLose remove mock repository\n")
}

// Clone : clone the git repository
func (r *MockRepository) Clone() (path string, err error) {
	log.Printf("git clone mock repository\n")
	return path, err
}

// Pull : pull the git repository
func (r *MockRepository) Pull() (err error) {
	log.Printf("git pull mock repository\n")
	return err
}

// Commit : commit the git repository
func (r *MockRepository) Commit(msg string) (hash string, err error) {
	log.Printf("git commit mock repository\n")
	return "", err
}
