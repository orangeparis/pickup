package repository_test

import (
	"testing"

	"bitbucket.org/orangeparis/pickup/repository"
)

func TestMockRepository(t *testing.T) {

	r := repository.NewMockRepository("./")
	repository.SetRepository(r)

	_, _ = repository.Clone()

	_ = repository.Pull()

	root := repository.Root()
	_ = root

	repository.Close()

}
