package repository

import (
	"io/ioutil"
	"log"
	"os"
	"time"

	"bitbucket.org/orangeparis/pickup/config"

	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"

	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
)

/*
	implements a minimal git repository
		Root()
		Close()
		Clone() (path string, err error) // Clone the repository, return the temporary path
		Pull() error                     // update the repository
		../..

	dependancies:
	 go get -u gopkg.in/src-d/go-git.v4/...

*/

// BaseRepository : interface to base repository
// type BaseRepository interface {
// 	Close()
// 	Clone() (path string, err error) // Clone the repository, return the temporary path
// 	Pull() error                     // update the repository
// }

// BaseGitRepository : implementation of a git Base repository
type BaseGitRepository struct {
	Url      string // repository url
	Path     string // target directory to clone the repository , each subdiretory is a platform
	User     string
	Password string

	// Author data for commit
	AuthorName  string
	AuthorEmail string
}

// NewGitRepository : create a base git repository handler
func NewBaseGitRepository(url string, path string, user string, password string) (repository *BaseGitRepository) {
	repository = &BaseGitRepository{Url: url, Path: path, User: user, Password: password}
	return repository
}

// NewBaseGitRepositoryFromConfig : create a git repository handler from config
func NewBaseGitRepositoryFromConfig() (repository *BaseGitRepository) {
	repository = NewBaseGitRepository(
		config.GetString("repository.url"),
		config.GetString("vars.tmp"),
		config.GetString("repository.user"),
		config.GetString("repository.password"))
	repository.AuthorName = config.GetString("repository.author")
	repository.AuthorEmail = config.GetString("repository.email")
	return repository
}

//
// implements BaseGitRepository interface
//

// Root :  return the root path ( where files where cloned)
func (r *BaseGitRepository) Root() (path string) {
	return r.Path
}

// Close : remove temprary dir
func (r *BaseGitRepository) Close() {
	log.Printf("remove git repository at %s\n", r.Path)
	os.RemoveAll(r.Path) // clean up
}

// Clone : clone the git repository in memory
func (r *BaseGitRepository) Clone() (path string, err error) {
	log.Printf("git clone %s\n", r.Url)
	options := &git.CloneOptions{
		URL: r.Url}
	if r.User != "" {
		// if user is specified add credentials to options
		auth := &http.BasicAuth{
			Username: r.User,
			Password: r.Password,
		}
		options.Auth = auth
	}
	// compute a temporary directory for repository
	dir, err := ioutil.TempDir("/tmp", "git")
	if err != nil {
		return "", (err)
	}
	r.Path = dir

	repository, err := git.PlainClone(r.Path, false, options)

	if err == nil {
		ref, err := repository.Head()
		if err != nil {
			log.Println(err.Error())
		} else {
			log.Printf("cloned to %s\n", r.Path)
			log.Printf("tag: %s\n", ref.Hash())
		}
	}
	return r.Path, err
}

// Pull : pull the git repository
func (r *BaseGitRepository) Pull() (err error) {
	log.Printf("git pull %s to %s\n", r.Url, r.Path)
	repository, err := git.PlainOpen(r.Path)
	if err != nil {
		return err
	}
	worktree, err := repository.Worktree()
	options := &git.PullOptions{}
	err = worktree.Pull(options)
	if err != nil {
		if err.Error() == "already up-to-date" {
			// ignore error
			log.Printf("Warning git already up-to-date")
			return nil
		}
	}
	return err
}

// Commit : commit the git repository
func (r *BaseGitRepository) Commit(msg string) (hash string, err error) {
	log.Printf("git commit %s to %s\n", r.Url, r.Path)
	repository, err := git.PlainOpen(r.Path)
	if err != nil {
		return "", err
	}
	worktree, err := repository.Worktree()

	// Commit(msg string, opts *CommitOptions) (plumbing.Hash, error)
	sig := &object.Signature{
		// Name represents a person name. It is an arbitrary string.
		Name: r.AuthorName,
		// Email is an email, but it cannot be assumed to be well-formed.
		Email: r.AuthorEmail,
		// When is the timestamp of the signature.
		When: time.Now(),
	}

	options := &git.CommitOptions{Author: sig}
	ghash, err := worktree.Commit(msg, options)
	if err != nil {
		if err.Error() == "already up-to-date" {
			// ignore error
			log.Printf("Warning git already up-to-date")
			return "", nil
		}
		log.Printf("error: %s\n", err.Error())
	}
	hash = ghash.String()
	log.Printf("repository has been commit with hash: %s\n", hash)
	return hash, err
}
