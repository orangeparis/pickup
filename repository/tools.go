package repository

import (
	"io/ioutil"
	"os"
)

// IOReadDir : return list of subdirectories ( exclude .* )
func IOReadDir(root string) ([]string, error) {
	var files []string
	fileInfo, err := ioutil.ReadDir(root)
	if err != nil {
		return files, err
	}
	for _, file := range fileInfo {
		name := file.Name()
		if name[0] == '.' {
			// skip .*
			continue
		}
		if file.IsDir() {
			// keep only directories
			files = append(files, name)
		}
	}
	return files, nil
}

// Exists reports whether the named file or directory exists.
func Exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}
