package pickup

/*

   domain for pickup application

*/

// api

// type Store interface {
// 	GetSheet(ctx context.Context, key string)
// 	GetSnapShot(ctx context.Context, key string)
// 	GetSheetList(ctx context.Context, key string)
// 	StoreSheet(ctx context.Context, key string, data []byte)
// 	StoreSnapShot(ctx context.Context, key string, data []byte)
// }

// Row : a Row from a sheet
type Row interface {
	ToJson() []byte
	ToCsv(rune) string
}

// Sheet : an array of row
type Sheet interface {
	LoadFromCsv(data []byte, sep rune) (err error)
	ToCsv(sep rune) (data string, err error)
	ToJson() (data []byte, err error)
}
