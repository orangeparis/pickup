package db

import (
	"context"

	"bitbucket.org/orangeparis/pickup"
	//"github.com/tinrab/meower/schema"
)

// Repository api storage
//type Repository interface {
type DataSheet interface {
	Close()

	GetSheet(ctx context.Context, key string) (sheet pickup.Sheet, err error)
	//GetSnapShot(ctx context.Context, key string)
	//GetSheetList(ctx context.Context, key string)
	//StoreSheet(ctx context.Context, key string, data []byte)
	//StoreSnapShot(ctx context.Context, key string, data []byte)

}

// local variable for injection dependency
var impl DataSheet

// SetRepository : setting the repository ( dependecy injection )
func SetDataSheet(sheet DataSheet) {
	impl = sheet
}

// Close Close the Repository
func Close() {
	impl.Close()
}

// GetSheet : get sheet by key
func GetSheet(ctx context.Context, key string) (sheet pickup.Sheet, err error) {
	return impl.GetSheet(ctx, key)
}
