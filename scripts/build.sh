
rm -rf ./ines
# compile 
# CGO_ENABLED=1 GOOS=linux GOARCH=amd64
export CGO_ENABLED=0
export GOARCH=amd64
# linux
export GOOS=linux
go build -v -o ./ines/pickup-$GOOS-$GOARCH ../cmd/pickup/main.go
# windows
export GOOS=windows
go build -v -o ./ines/pickup-$GOOS-$GOARCH.exe ../cmd/pickup/main.go
# add elements
cp ../cmd/pickup/config.toml ./ines
cp ./config.toml ./ines
cp -R ../web/static ./ines
cp -R ../web/templates ./ines
# build the zip
cd ines; zip -r ines.zip *
# compile to osx
cd ..
export GOOS=darwin
go build -o ./ines/pickup-$GOOS-$GOARCH ../cmd/pickup/main.go

