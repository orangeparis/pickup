package pickup_test

import (
	"io/ioutil"
	"log"
	"testing"

	"bitbucket.org/orangeparis/pickup"
)

var liveboxFilename = "./mock/infra_labo.csv"

func TestLiveboxSheet2(t *testing.T) {

	data, err := ioutil.ReadFile(liveboxFilename)
	if err != nil {
		log.Fatal(err)
	}

	sheet, _ := pickup.NewLiveboxSheet2("sampleLivebox")
	name := sheet.GetName()
	_ = name
	err = sheet.LoadFromCsv(data, ';')
	if err != nil {
		log.Fatal(err)
	}

	data, err = sheet.ToJson()
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(string(data))

	buffer, err := sheet.ToCsv(';')
	if err != nil {
		log.Fatal(err)
	}
	_ = buffer
	//fmt.Println(buffer)

	// compute headers size
	sizes := sheet.SizeHeaders(10)
	_ = sizes
	//fmt.Println(sizes)

	sheet.AddLeftColumn("Select", "")
	buffer, err = sheet.ToCsv(';')
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(buffer)

	if len(sheet.Headers) != 10 {
		t.Fail()
		return
	}

	sheet.DeleteLeftColumn()
	if len(sheet.Headers) != 9 {
		t.Fail()
		return
	}

}
