package config

import (
	"fmt"

	"github.com/spf13/viper"
)

type Configuration struct {
	Name string
}

func init() {

	// defaults
	viper.SetDefault("appName", "ines")
	viper.SetDefault("RepositoryDir", "/tmp")
	viper.SetDefault("RepositoryPrefix", "ines") // temporary repository dir : /tmp/ines*

	// [repository]
	viper.SetDefault("repository.url", "")
	viper.SetDefault("repository.user", "")
	viper.SetDefault("repository.password", "")
	viper.SetDefault("repository.author", "ines")
	viper.SetDefault("repository.email", "ines@orange.com")

	// [web]
	viper.SetDefault("web.port", "8081")
	viper.SetDefault("web.static", "./static")
	viper.SetDefault("web.templates", "./templates")
	//viper.SetDefault("Taxonomies", map[string]string{"tag": "tags", "category": "categories"})

	// config files
	viper.SetConfigName("config")      // name of config file (without extension)
	viper.AddConfigPath("/etc/ines/")  // path to look for the config file in
	viper.AddConfigPath("$HOME/.ines") // call multiple times to add many search paths
	viper.AddConfigPath(".")           // optionally look for config in the working directory

	// Env vars
	viper.SetEnvPrefix("ines")

}

// Load : read the config files
func Load() {

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

}

// Get : Get config key
func Get(key string) interface{} {
	return viper.Get(key)
}

// Set config key with value
func Set(key string, value interface{}) {
	viper.Set(key, value)
}

// GetString : get key as string
func GetString(key string) string {
	return viper.GetString(key)
}
