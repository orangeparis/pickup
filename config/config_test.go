package config_test

import (
	"testing"
	// "github.com/spf13/viper"
	"bitbucket.org/orangeparis/pickup/config"
	"github.com/spf13/viper"
)

func TestConfig(t *testing.T) {

	config.Load()
	data := viper.AllSettings()
	_ = data
	repo := viper.GetString("repository.url")
	println(repo)
	// if repo != "/tmp/ines" {
	// 	t.Errorf("repo was incorrect, got: %s, want: %s", repo, "/tmp/ines")
	// }

	tmp := viper.GetString("vars.tmp")
	println(tmp)
	if tmp != "/tmp/ines" {
		t.Errorf("tmp was incorrect, got: %s, want: %s", tmp, "/tmp/ines")
	}

	// setting vars.tmp
	viper.Set("vars.tmp", "/tmp/ines_modified")

	tmp = viper.GetString("vars.tmp")
	println(tmp)
	if tmp != "/tmp/ines_modified" {
		t.Errorf("repo was incorrect, got: %s, want: %s", tmp, "/tmp/ines_modified")
	}

	println(viper.GetString("appName"))

}
