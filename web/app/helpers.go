package app

import (
	"os"
	"path/filepath"

	"bitbucket.org/orangeparis/pickup/config"
)

func StaticPath(name string) (path string) {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	//fmt.Println(exPath)
	static := config.GetString("web.static")
	path = filepath.Join(exPath, static, name)
	//path = exPath + "/ " + static + "/" + name
	//fmt.Println(path)
	return path
}

// TemplatePath compute template path with config
func TemplatePath(name string) (path string) {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	//fmt.Printf("expath:%s\n", exPath)
	tmpl := config.GetString("web.templates")
	if name == "" {
		path = filepath.Join(exPath, tmpl)
	} else {
		name = name + ".html"
		path = filepath.Join(exPath, tmpl, name)
	}
	return path
}
