package app

import "net/http"

func Routes() {

	//
	// static file server
	//
	// This works and strip "/static/" fragment from path
	fs := http.FileServer(http.Dir(StaticPath("")))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	//
	// home
	//
	http.HandleFunc("/", HomeHandler)

	//
	// livebox
	//

	// return list of platforms
	http.HandleFunc("/api/platforms", apiPlatformsHandler)
	// return livebox csv content of the platform
	http.HandleFunc("/api/livebox/", apiLiveboxCsvHandler)
	// return livebox shhet page
	http.HandleFunc("/sheet/livebox/", liveboxSheetHandler)

	//
	// vm
	//

	// list of vm platforms
	http.HandleFunc("/api/vmplatforms", apiVmPlatformsHandler)

	// return vm csv content of the platform
	http.HandleFunc("/api/vm/", apiVmCsvHandler)

	// vm sheet page
	http.HandleFunc("/sheet/vm/", vmSheetHandler)

}
