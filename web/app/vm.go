package app

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"

	"bitbucket.org/orangeparis/pickup"

	// repository injection dependency
	vmrepo "bitbucket.org/orangeparis/pickup/repository/vm"
)

/*

	controlers for vm

	/api/vmplatforms
	/api/vm/*
	/sheet/vm/*


*/

//
// vm api
//
//	/api/vmplatforms
//  /api/vm/<ptf>
//

// handle /api/vmplatforms return a json list of vm platforms
func apiVmPlatformsHandler(w http.ResponseWriter, r *http.Request) {

	platforms := vmrepo.Platforms()

	js, err := json.Marshal(platforms)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)

}

// apiVmCsvHandler handle /api/vm/<ptf> return a canonical csv ( comma separated)
func apiVmCsvHandler(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Path[len("/api/vm/"):]
	csv, err := vmrepo.GetPlatform(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	//_ = cvs

	// build vm sheet from platform info
	sheet, err := pickup.NewVm2s(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	err = sheet.LoadFromCsv(csv, ';')
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}
	// add Select column
	sheet.AddLeftColumn("Select", "")
	buffer, err := sheet.ToCsv(',')
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(buffer))

}

//
//	vm sheets
//
//    /sheet/vm/<ptf>
//

// handle /sheet/vm/<ptf>
func vmSheetHandler(w http.ResponseWriter, r *http.Request) {

	ptf := r.URL.Path[len("/sheet/vm/"):]
	platform_data, err := vmrepo.GetPlatform(ptf)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	_ = platform_data

	vmo, err := pickup.NewVm2s(ptf)
	vmo.LoadFromCsv(platform_data, ';')
	// add select column
	vmo.AddLeftColumn("Select", "")

	headers := vmo.Headers
	sizes := vmo.SizeHeaders(10)

	data := struct {
		Platform string
		Headers  []string
		Sizes    []int
		//PlatFormCsv []byte
		//Items    []string
	}{
		Platform: ptf,
		Headers:  headers,
		Sizes:    sizes,
		// 	"My blog",
		// },
	}

	t, err := template.New("vm.html").ParseFiles(TemplatePath("vm"))
	if err != nil {
		log.Print("template compiling error: ", err)
	}
	err = t.Execute(w, data)
	if err != nil {
		log.Print("template executing error: ", err)
	}
	//fmt.Fprintf(w, "Hi from sheet handler, I love %s!", r.URL.Path[1:])
}
