package pickup_test

import (
	//"fmt"
	"io/ioutil"
	"log"
	"testing"

	"bitbucket.org/orangeparis/pickup"
)

//var filename = "./mock/infra_labo.csv"
var filename = "./mock/infra_labo.csv"

func LoadFromCsv(s pickup.Sheet, data []byte, sep rune) (err error) {
	return s.LoadFromCsv(data, ';')
}

func ExportToJson(s pickup.Sheet) (data []byte, err error) {
	return s.ToJson()
}

func ExportToCsv(s pickup.Sheet, sep rune) (data string, err error) {
	return s.ToCsv(sep)
}

func TestSheet(*testing.T) {

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	sheet, err := pickup.NewLiveboxSheet2("demo")

	err = LoadFromCsv(sheet, data, ';')
	if err != nil {
		log.Fatal(err)
	}

	// err = sheet.LoadFromCsv(data)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	data, err = ExportToJson(sheet)
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(string(data))

	buffer, err := ExportToCsv(sheet, ';')
	if err != nil {
		log.Fatal(err)
	}
	_ = buffer
	//fmt.Println(buffer)

}
