package pickup

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strings"
)

// describes Livebox records and sheet

var liveboxHeaders = "J;Livebox;Brass;BrassEX;Ip;Type;key;MAC;Passwd"

// LiveboxSheet : implements Sheet (LoadFromCscv , ToJson , ToCsv)
type LiveboxSheet struct {
	Key         string // platform name
	Description string // description
	Row         []LiveboxRow
	headers     []string
}

// implements Sheet

// NewLiveboxSheet : create a livebox Sheet
func NewLiveboxSheet(name string, description string) (sheet *LiveboxSheet, err error) {

	s := &LiveboxSheet{Key: name, Description: description}
	s.headers = strings.Split(liveboxHeaders, ";")
	return sheet, err
}

// LoadFromCsv : init rows with csv data
func (s *LiveboxSheet) LoadFromCsv(data []byte, sep rune) (err error) {
	//s.Row = []LiveboxRow
	if sep != ';' {
		sep = ','
	}

	r := csv.NewReader(strings.NewReader(string(data)))
	r.Comma = sep
	r.Comment = '#'
	r.FieldsPerRecord = 14

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Print(records)
	group := ""
	for _, line := range records {

		if group == "" {
			// find a group :( first element is J1 or  J2 ..)
			if g := groupMark(line[0]); g == "" {
				// go group found : skip line
				continue
			} else {
				// group found
				group = g
				s.addLine(line, group)
			}
		} else {
			// inside a group
			// check new group marker
			if g := groupMark(line[0]); g != "" {
				// a new group indication , update it
				group = g
			}
			s.addLine(line, group)
		}
		//fmt.Println(line)
	}
	return err
}

func (s *LiveboxSheet) ToArray() (data [][]string) {
	for _, row := range s.Row {
		data = append(data, row.ToArray())
	}
	return data
}

// ToCsv : export to csv
func (s *LiveboxSheet) ToCsv(sep rune) (data string, err error) {

	var buffer []string
	for _, row := range s.Row {

		r, err := row.ToCsv(sep)
		if err != nil {
			return data, err
		}
		buffer = append(buffer, r)

	}
	data = strings.Join(buffer, "")
	return data, err
}

// ToFullJson : export to Full Json
func (s *LiveboxSheet) ToFullJson() (data []byte, err error) {
	data, err = json.Marshal(s.Row)
	return data, err
}

// TOJson : export as a json array ( no keys)
func (s *LiveboxSheet) ToJson() (data []byte, err error) {
	// to json array
	data = append(data, '[')
	for _, row := range s.Row {
		e, err := row.ToJson()
		if err != nil {
			return data, err
		}
		data = append(data, e...)
		data = append(data, ',')
	}
	// replace the last , with a ]
	data[len(data)-1:][0] = ']'
	return data, err
}

// Headers : return a array of headers
func (s *LiveboxSheet) Headers() (headers []string, err error) {
	return s.headers, nil
}

func (s *LiveboxSheet) addLine(line []string, group string) (err error) {

	row, err := NewLiveboxRow(line, group)
	if err != nil {
		return err
	}
	s.Row = append(s.Row, row)
	return err
}

//
// handle Livebox Row
//

// ;Livebox;     Brassage LAN; Brassage EX;   IP Admin";"    Type";"    " Serial Number"; Adresse MAC;       Clé WiFi     ; Clé H235      ;AuthCryptPsswd;;;
// Livebox-5F38; B5-21-02;     EX4300 -00-02; 192.168.200.3; Livebox 3; LK17126DP160920;  C0:D0:44:D8:5F:38; 3942FCF6;;;;;

// LiveboxRow : a line of livebox sheet
type LiveboxRow struct {
	Group         string // J1 / J2 / J3 / J4
	Name          string //  livebox name   : Livebox-5F38
	BrassageLan   string // Lan Brassage    : B5-21-02
	BrassageEx    string // Brassage EX     : EX4300 -00-02
	Ip            string // Ip admin        : 192.168.200.3
	Serial        string // Serial Number   : LK17126DP160920
	Mac           string // Mac Addressv    : C0:D0:44:D8:5F:38
	Wifi          string // Wifi Key        : 3942FCF6
	H235          string // H235 Key
	CryptPassword string // AuthCryptPsswd
}

func (s *LiveboxRow) ToArray() (data []string) {
	data = append(data, s.Group)
	data = append(data, s.Name)
	data = append(data, s.BrassageLan)
	data = append(data, s.BrassageEx)
	data = append(data, s.Ip)
	data = append(data, s.Serial)
	data = append(data, s.Mac)
	data = append(data, s.Wifi)
	data = append(data, s.H235)
	data = append(data, s.CryptPassword)
	// fill up to 14 fields
	data = append(data, "")
	data = append(data, "")
	data = append(data, "")
	data = append(data, "")
	return data
}

// ToFullJson : export to Json
func (s *LiveboxRow) ToFullJson() (data []byte, err error) {
	data, err = json.Marshal(data)
	return data, err
}

// ToCsv : export to csv
func (s *LiveboxRow) ToCsv(sep rune) (data string, err error) {

	if sep != ';' {
		sep = ','
	}
	var in [][]string
	in = append(in, s.ToArray())
	out := &strings.Builder{}
	w := csv.NewWriter(out)
	//w.Comma = ','
	w.Comma = sep

	w.WriteAll(in) // calls Flush internally
	if err := w.Error(); err != nil {
		log.Fatalln("error writing csv:", err)
	}
	//fmt.Println(out.String())
	data = out.String()

	return data, err

}

// ToJsonArray : export to Json
func (s *LiveboxRow) ToJson() (data []byte, err error) {
	// export to json as an array ( without keys )
	a := s.ToArray()
	data, err = json.Marshal(a)
	return data, err
}

func NewLiveboxRow(line []string, group string) (row LiveboxRow, err error) {

	r := LiveboxRow{
		Group: group,
		Name:  line[1], BrassageLan: line[2], BrassageEx: line[3],
		Ip: line[4], Serial: line[5], Mac: line[6],
		Wifi: line[7], H235: line[8], CryptPassword: line[9],
	}

	if len(r.Name) == 0 {
		err = errors.New("Name cannot be empty")
		return r, err
	}

	return r, err
}

// return the groupMark or space of row of livebox ( Jx )
func groupMark(text string) (group string) {
	group = ""
	// see if text is a group mark ( J\n1 , J\2 ...)
	if len(text) >= 3 {
		if text[0] == 'J' {
			if text[1] == '\n' {
				// this is group mark in original field
				group = fmt.Sprintf("%s%s", string(text[0]), string(text[2]))
				return group
			}
		}
	}
	if len(text) >= 2 {
		if text[0] == 'J' {
			// this is group mark in canonical fields
			group = text
		}
	}
	return group
}
