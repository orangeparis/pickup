package pickup_test

import (
	"fmt"
	"io/ioutil"
	"log"
	"testing"

	"bitbucket.org/orangeparis/pickup"
)

var filename = "./mock/infra_labo.csv"

var csv_canonic = `J1;Livebox-5D2E;B5-21-01;EX4300 -00-01;192.168.200.2;Livebox 3;LK17126DP160833;C0:D0:44:D8:5D:2E;C9E47F99;;;;;
J1;Livebox-5F38;B5-21-02;EX4300 -00-02;192.168.200.3;Livebox 3;LK17126DP160920;C0:D0:44:D8:5F:38;3942FCF6;;;;;
J1;Livebox-5E0C;B5-21-03;EX4300 -00-03;192.168.200.4;Livebox 3;LK17126DP160870;C0:D0:44:D8:5E:0C;E656633E;;;;;
J1;Livebox-602E;B5-21-04;EX4300 -00-04;192.168.200.5;Livebox 3;LK17126DP160961;C0:D0:44:D8:60:2E;DCFA6154;;;;;
J1;Livebox-60FA;B5-21-05;EX4300 -00-05;192.168.200.6;Livebox 3;LK17126DP160995;C0:D0:44:D8:60:FA;37CA36E1;;;;;
`

func TestLoadLiveboxSheet(*testing.T) {

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	sheet := &pickup.LiveboxSheet{}
	err = sheet.LoadFromCsv(data, ';')
	if err != nil {
		log.Fatal(err)
	}

	data, err = sheet.ToJson()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(data))

	buffer, err := sheet.ToCsv(';')
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(buffer)

}

func TestLoadCanonicLiveboxSheet(*testing.T) {

	data := []byte(csv_canonic)

	sheet := &pickup.LiveboxSheet{}
	err := sheet.LoadFromCsv(data, ';')
	if err != nil {
		log.Fatal(err)
	}

	data, err = sheet.ToFullJson()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(data))

	data, err = sheet.ToJson()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(data))

	buffer, err := sheet.ToCsv(';')
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(buffer)

	// extract first row
	first := sheet.Row[0]
	csv, err := first.ToCsv(';')
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(csv)

}
