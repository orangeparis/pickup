package pickup

import (
	"encoding/json"
	"log"

	"bitbucket.org/orangeparis/pickup/tools"
)

/*

	describe vm repository structure

*/

//var vmHeaders = "Force;NameVM;vCenter;vCenterUser;vCenterPass;Cluster;ESX;Template;TemplateNIC;TemplateNbNIC;DefautLanNIC;TemplateUser;TemplatePwd;CPU;RAM;HDD1Datastore;HDD1SuppSize;HDD1SuppDatastore;HDD2Size;HDD2Datastore;HDD3Size;HDD3Datastore;VMIP;VMMask;VMGTW;DNSSEARCH;DNSNAME1;DNSNAME2;LAN1;IP1;Mask1;GTW1;LAN2;IP2;Mask2;GTW2;LAN3;IP3;Mask3;GTW3;Lan4;IP4;Mask4;GTW4;LAN5;IP5;Mask5;GTW5;LAN6;IP6;Mask6;GTW6"

type Vm2s struct {
	*tools.CsvTable

	Name string // Name of the collection
	Key  string
}

func NewVm2s(name string) (vms *Vm2s, err error) {

	table := &tools.CsvTable{}
	vms = &Vm2s{CsvTable: table, Name: name, Key: "NameVm"}
	return vms, err
}

// ToJson : intercept CvvTable.ToJson()
func (v *Vm2s) ToJson() (data []byte, err error) {
	log.Printf("Vm2 Interception\n")
	return json.Marshal(v.Lines)
}

// ToJson : intercept CvvTable.ToJson()
func (v *Vm2s) GetName() string {
	return v.Name
}
