package pickup

/*

the data contained in platform subdirectory of a repository

repository structure

ines-deployment    <- root of repository
	infra          <- infra subdirectory
		env_ines_integration_2a2   <- platform


platform structure

env_ines_integration_2a2      <- platform subdirectory
	files                     <- files subdirectory
		livebox.csv			  <- csv file describing livebox configurations

*/

// Platform : raw platform parameters
type Platform struct {
	Name       string // get the platforms Name   ( env_ines_integration_2a2 )
	LiveboxCsv []byte // get the livebox csv content
	//Adress()   string    // get the platform address

}

// GetLiveboxSheet : return a liveboxSheet with csv imported
// func (p *Platform) GetLiveboxSheet() (sheet Sheet, err error) {

// 	sheet = &LiveboxSheet2{Key: p.Name, }
// 	err = sheet.LoadFromCsv(p.LiveboxCsv, ';')
// 	if err != nil {
// 		return sheet, err
// 	}
// 	return sheet, err
// }
