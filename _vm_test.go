package pickup_test

import (
	"fmt"
	"io/ioutil"
	"log"
	"testing"

	"bitbucket.org/orangeparis/pickup"
)

var vmfilename = "./mock/deployvm.csv"

func TestLoadVmSheet(*testing.T) {

	data, err := ioutil.ReadFile(vmfilename)
	if err != nil {
		log.Fatal(err)
	}

	sheet, _ := pickup.NewVms("samplevm")
	err = sheet.LoadFromCsv(data, ';')
	if err != nil {
		log.Fatal(err)
	}

	data, err = sheet.ToJson()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(data))

	buffer, err := sheet.ToCsv(';')
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(buffer)

	// compute headers size
	sizes := sheet.SizeHeaders()
	fmt.Println(sizes)

}
