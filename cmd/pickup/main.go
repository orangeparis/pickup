package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"bitbucket.org/orangeparis/pickup/config"
	lbrepo "bitbucket.org/orangeparis/pickup/repository/livebox"
	vmrepo "bitbucket.org/orangeparis/pickup/repository/vm"
	"bitbucket.org/orangeparis/pickup/web/app"
)

var templates *template.Template

func main() {

	// get config
	config.Load()
	//config.Set("static", "../../web/static")
	//config.Set("templates", "../../web/templates")

	// set the livebox repository
	r := lbrepo.NewGitRepositoryFromConfig()
	path, err := r.Clone()
	if err != nil {
		log.Printf("Error setting livebox repository: %s\n", err.Error())
		log.Fatal(err)
	}
	// update the temporary path in config
	config.Set("vars.tmp", path)
	lbrepo.SetRepository(r)
	defer lbrepo.Close()

	// set the Vm repository
	r2 := vmrepo.NewGitRepositoryFromConfig()
	path, err = r2.Clone()
	if err != nil {
		log.Printf("Error setting vm repository: %s\n", err.Error())
		log.Fatal(err)
	}
	vmrepo.SetRepository(r2)
	defer vmrepo.Close()

	// declare web applcation routes
	app.Routes()

	// starts the web server
	port := config.GetString("web.port")
	addr := fmt.Sprintf(":%s", port)

	log.Printf("start server at %s", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
