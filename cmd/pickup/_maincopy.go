package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"bitbucket.org/orangeparis/pickup"

	"bitbucket.org/orangeparis/pickup/config"
	repository "bitbucket.org/orangeparis/pickup/repository/livebox"
	vmrepo "bitbucket.org/orangeparis/pickup/repository/vm"
)

// func viewHandler(w http.ResponseWriter, r *http.Request) {
// 	title := r.URL.Path[len("/view/"):]
// 	fmt.Fprintf(w, "<h1>%s</h1><div>%s</div>", "view", title)
// }

func handler(w http.ResponseWriter, r *http.Request) {

	t, err := template.New("home.html").ParseFiles(TemplatePath("home"))
	if err != nil {
		log.Print("template compiling error: ", err)
	}
	data := struct {
		Repository       string
		LiveboxPlatforms []string
		VmPlatforms      []string
	}{
		Repository:       "ines",
		LiveboxPlatforms: repository.Platforms(),
		VmPlatforms:      vmrepo.Platforms(),
	}

	err = t.Execute(w, data)
	if err != nil {
		log.Print("template executing error: ", err)
	}

	//fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}

// // handle /sheet/
// func sheetHandler(w http.ResponseWriter, r *http.Request) {

// 	err := templates.Execute(w, "livebox.html")
// 	if err != nil {
// 		log.Print("template executing error: ", err)
// 	}
// 	//fmt.Fprintf(w, "Hi from sheet handler, I love %s!", r.URL.Path[1:])
// }

// handle /sheet/livebox/<ptf>
func liveboxSheetHandler(w http.ResponseWriter, r *http.Request) {

	ptf := r.URL.Path[len("/sheet/livebox/"):]
	platform_data, err := repository.GetPlatform(ptf)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	sheet, err := pickup.NewLiveboxSheet2(ptf)
	sheet.LoadFromCsv(platform_data, ';')
	// add select column
	sheet.AddLeftColumn("Select", "")

	headers := sheet.Headers
	sizes := sheet.SizeHeaders(10)

	data := struct {
		Platform string
		Headers  []string
		Sizes    []int
	}{
		Platform: ptf,
		Headers:  headers,
		Sizes:    sizes,
	}

	t, err := template.New("livebox.html").ParseFiles(TemplatePath("livebox"))
	if err != nil {
		log.Print("template compiling error: ", err)
	}
	err = t.Execute(w, data)
	if err != nil {
		log.Print("template executing error: ", err)
	}
	//fmt.Fprintf(w, "Hi from sheet handler, I love %s!", r.URL.Path[1:])
}

// handle /api/livebox/<ptf> return a canonical csv ( comma separated)
func apiLiveboxCsvHandler(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Path[len("/api/livebox/"):]
	csv, err := repository.GetPlatform(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// build livebox sheet from platform info
	sheet, err := pickup.NewLiveboxSheet2(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	err = sheet.LoadFromCsv(csv, ';')
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}
	// add Select column
	sheet.AddLeftColumn("Select", "")
	buffer, err := sheet.ToCsv(',')
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(buffer))

}

// handle /api/platforms return a json list of platforms
func apiPlatformsHandler(w http.ResponseWriter, r *http.Request) {

	platforms := repository.Platforms()

	js, err := json.Marshal(platforms)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)

}

// handle /api/platform/<name> return a json object
func apiPlatformHandler(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Path[len("/api/platform/"):]

	platform, err := repository.GetPlatform(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	js, err := json.Marshal(platform)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)

}

func StaticPath(name string) (path string) {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	//fmt.Println(exPath)
	static := config.GetString("web.static")
	path = filepath.Join(exPath, static, name)
	//path = exPath + "/ " + static + "/" + name
	//fmt.Println(path)
	return path
}

func TemplatePath(name string) (path string) {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	fmt.Printf("expath:%s\n", exPath)
	tmpl := config.GetString("web.templates")
	if name == "" {
		path = filepath.Join(exPath, tmpl)
	} else {
		name = name + ".html"
		path = filepath.Join(exPath, tmpl, name)
	}
	return path
}

//
// vm sheet
//

// handle /api/vmplatforms return a json list of vm platforms
func apiVmPlatformsHandler(w http.ResponseWriter, r *http.Request) {

	platforms := repository.Platforms()

	js, err := json.Marshal(platforms)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)

}

// handle /sheet/vm/<ptf>
func vmSheetHandler(w http.ResponseWriter, r *http.Request) {

	ptf := r.URL.Path[len("/sheet/vm/"):]
	platform_data, err := vmrepo.GetPlatform(ptf)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	_ = platform_data

	vmo, err := pickup.NewVm2s(ptf)
	vmo.LoadFromCsv(platform_data, ';')
	// add select column
	vmo.AddLeftColumn("Select", "")

	headers := vmo.Headers
	sizes := vmo.SizeHeaders(10)

	data := struct {
		Platform string
		Headers  []string
		Sizes    []int
		//PlatFormCsv []byte
		//Items    []string
	}{
		Platform: ptf,
		Headers:  headers,
		Sizes:    sizes,
		// 	"My blog",
		// },
	}

	t, err := template.New("vm.html").ParseFiles(TemplatePath("vm"))
	if err != nil {
		log.Print("template compiling error: ", err)
	}
	err = t.Execute(w, data)
	if err != nil {
		log.Print("template executing error: ", err)
	}
	//fmt.Fprintf(w, "Hi from sheet handler, I love %s!", r.URL.Path[1:])
}

// apiVmCsvHandler handle /api/vm/<ptf> return a canonical csv ( comma separated)
func apiVmCsvHandler(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Path[len("/api/vm/"):]
	csv, err := vmrepo.GetPlatform(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	//_ = cvs

	// build vm sheet from platform info
	sheet, err := pickup.NewVm2s(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	err = sheet.LoadFromCsv(csv, ';')
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}
	// add Select column
	sheet.AddLeftColumn("Select", "")
	buffer, err := sheet.ToCsv(',')
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(buffer))

}

var templates *template.Template

func main() {

	// get config
	config.Load()
	config.Set("static", "../../web/static")
	config.Set("templates", "../../web/templates")

	// println(StaticPath(""))
	// println(StaticPath("js/data.csv"))
	// println(TemplatePath(""))

	// set a livebox repository
	r := repository.NewGitRepositoryFromConfig()
	path, err := r.Clone()
	if err != nil {
		log.Printf("Error setting livebox repository: %s\n", err.Error())
		log.Fatal(err)
	}
	// update the temporary path in config
	config.Set("vars.tmp", path)
	repository.SetRepository(r)
	defer repository.Close()

	// set Vm repository
	r2 := vmrepo.NewGitRepositoryFromConfig()
	path, err = r2.Clone()
	if err != nil {
		log.Printf("Error setting vm repository: %s\n", err.Error())
		log.Fatal(err)
	}
	vmrepo.SetRepository(r2)
	defer vmrepo.Close()

	// pls := repository.Platforms()
	// fmt.Println(pls)

	// // create repository : GetSheet() ...
	// store, err := db.NewMockLiveboxCsv()
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// // inject repository
	// db.SetDataSheet(store)
	// defer store.Close()

	// // test repo
	// s1, err := db.GetSheet(context.TODO(), "K1")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// text, err := s1.ToJson()
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Println(string(text))

	// fmt.Println(s1.ToCsv(';'))

	//
	// static file server
	//
	// This works and strip "/static/" fragment from path
	fs := http.FileServer(http.Dir(StaticPath("")))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	// setup template

	templates = template.Must(template.ParseFiles(TemplatePath("livebox")))
	_ = templates

	http.HandleFunc("/sheet/livebox/", liveboxSheetHandler)
	http.HandleFunc("/sheet/vm/", vmSheetHandler)
	//http.HandleFunc("/sheet/", sheetHandler)

	//http.HandleFunc("/view/", viewHandler)

	// return list of platforms
	http.HandleFunc("/api/platforms", apiPlatformsHandler)
	http.HandleFunc("/api/vmplatforms", apiVmPlatformsHandler)
	// return json object of the platform
	http.HandleFunc("/api/platform/", apiPlatformHandler)

	// return livebox csv content of the platform
	http.HandleFunc("/api/livebox/", apiLiveboxCsvHandler)
	// return vm csv content of the platform
	http.HandleFunc("/api/vm/", apiVmCsvHandler)

	//http.HandleFunc("/api/", apiHandler)
	http.HandleFunc("/", handler)

	port := config.GetString("web.port")
	addr := fmt.Sprintf(":%s", port)

	log.Printf("start server at %s", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
