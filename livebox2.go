package pickup

import (
	"encoding/csv"
	"fmt"
	"log"
	"strings"

	"bitbucket.org/orangeparis/pickup/tools"
)

/*

	describe livebox repository structure

*/

var defaultLiveboxHeaders = []string{"J", "Livebox", "Brass", "BrassEX", "Ip", "Type", "key", "MAC", "Passwd"}

//var vmHeaders = "Force;NameVM;vCenter;vCenterUser;vCenterPass;Cluster;ESX;Template;TemplateNIC;TemplateNbNIC;DefautLanNIC;TemplateUser;TemplatePwd;CPU;RAM;HDD1Datastore;HDD1SuppSize;HDD1SuppDatastore;HDD2Size;HDD2Datastore;HDD3Size;HDD3Datastore;VMIP;VMMask;VMGTW;DNSSEARCH;DNSNAME1;DNSNAME2;LAN1;IP1;Mask1;GTW1;LAN2;IP2;Mask2;GTW2;LAN3;IP3;Mask3;GTW3;Lan4;IP4;Mask4;GTW4;LAN5;IP5;Mask5;GTW5;LAN6;IP6;Mask6;GTW6"

type LiveboxSheet2 struct {
	*tools.CsvTable

	Name string // Name of the collection
	Key  string
}

// NewLiveboxSheet2 create Livebox Sheet
func NewLiveboxSheet2(name string) (vms *LiveboxSheet2, err error) {

	table := &tools.CsvTable{Headers: defaultLiveboxHeaders}
	vms = &LiveboxSheet2{CsvTable: table, Name: name, Key: "Livebox"}
	return vms, err
}

// LoadFromCsv : override CsvTable Load
func (s *LiveboxSheet2) LoadFromCsv(data []byte, sep rune) (err error) {
	//s.Row = []LiveboxRow
	if sep != ';' {
		sep = ','
	}

	r := csv.NewReader(strings.NewReader(string(data)))
	r.Comma = sep
	r.Comment = '#'
	r.FieldsPerRecord = 14

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Print(records)
	group := ""
	for _, line := range records {

		if group == "" {
			// find a group :( first element is J1 or  J2 ..)
			if g := groupTag(line[0]); g == "" {
				// go group found : skip line
				continue
			} else {
				// group found
				group = g
				s.addLine(line, group)
			}
		} else {
			// inside a group
			// check new group marker
			if g := groupTag(line[0]); g != "" {
				// a new group indication , update it
				group = g
			}
			s.addLine(line, group)
		}
		//fmt.Println(line)
	}
	return err
}

// GetName : return name
func (v *LiveboxSheet2) GetName() string {
	return v.Name
}

// helpers

func (s *LiveboxSheet2) addLine(line []string, group string) (err error) {
	line[0] = group
	s.Lines = append(s.Lines, line)
	return err
}

// return the groupMark or space of row of livebox ( Jx )
func groupTag(text string) (group string) {
	group = ""
	// see if text is a group mark ( J\n1 , J\2 ...)
	if len(text) >= 3 {
		if text[0] == 'J' {
			if text[1] == '\n' {
				// this is group mark in original field
				group = fmt.Sprintf("%s%s", string(text[0]), string(text[2]))
				return group
			}
		}
	}
	if len(text) >= 2 {
		if text[0] == 'J' {
			// this is group mark in canonical fields
			group = text
		}
	}
	return group
}
