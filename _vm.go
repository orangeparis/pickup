package pickup

import (
	"encoding/csv"
	"encoding/json"
	"io"
	"log"
	"strings"
)

/*

	describe vm repository structure

*/

var vmHeaders = "Force;NameVM;vCenter;vCenterUser;vCenterPass;Cluster;ESX;Template;TemplateNIC;TemplateNbNIC;DefautLanNIC;TemplateUser;TemplatePwd;CPU;RAM;HDD1Datastore;HDD1SuppSize;HDD1SuppDatastore;HDD2Size;HDD2Datastore;HDD3Size;HDD3Datastore;VMIP;VMMask;VMGTW;DNSSEARCH;DNSNAME1;DNSNAME2;LAN1;IP1;Mask1;GTW1;LAN2;IP2;Mask2;GTW2;LAN3;IP3;Mask3;GTW3;Lan4;IP4;Mask4;GTW4;LAN5;IP5;Mask5;GTW5;LAN6;IP6;Mask6;GTW6"

type Vms struct {
	Name string // Name of the collection

	RawHeaders string // raw csv header line ( like vmHeaders)

	Key string // name of the field for the key ( unique vm)  eg: NameVM

	Lines [][]string // lines

	headers    []string
	headerSize []int // a table of size for headers
}

func NewVms(name string) (vms *Vms, err error) {

	vms = &Vms{Name: name, RawHeaders: vmHeaders, Key: "NameVm"}
	vms.headers = strings.Split(vms.RawHeaders, ";")
	return vms, err
}

//
// implements  Sheet interface
//
// LoadFromCsv(data []byte, sep rune) (err error)
// ToCsv(sep rune) (data string, err error)
// ToJson() (data []byte, err error)

func (v *Vms) LoadFromCsv(data []byte, sep rune) (err error) {

	if sep != ';' {
		sep = ','
	}

	r := csv.NewReader(strings.NewReader(string(data)))
	r.Comma = sep
	r.Comment = '#'
	//r.FieldsPerRecord = len(v.headers)

	var headerFlag = -1 //   header status :  -1 not set, 0: no header , 1 : header found

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if headerFlag == -1 {
			// this is the first line
			if record[0] == "Force" {
				headerFlag = 1
				v.headers = record
				continue
			} else {
				headerFlag = 0
			}
		}
		// add line
		v.Lines = append(v.Lines, record)
	}

	return err
}

func (v *Vms) ToCsv(sep rune) (data string, err error) {

	if sep != ';' {
		sep = ','
	}
	in := v.Lines
	out := &strings.Builder{}
	w := csv.NewWriter(out)
	//w.Comma = ','
	w.Comma = sep

	w.WriteAll(in) // calls Flush internally
	if err := w.Error(); err != nil {
		log.Fatalln("error writing csv:", err)
	}
	//fmt.Println(out.String())
	data = out.String()

	return data, err
}

func (v *Vms) ToJson() (data []byte, err error) {
	return json.Marshal(v.Lines)
}

// other methods

// GetHeaders
func (v *Vms) Headers() (headers []string, err error) {
	return v.headers, nil
}

// SizeHeaders : compute headers size
func (v *Vms) SizeHeaders() (headerSizes []int) {

	// coef for ratio text lenght -> pixel
	coef := 10

	for i, header := range v.headers {
		// minimum size of header is len of the label
		min := len(header)
		headerSizes = append(headerSizes, min)
		for _, line := range v.Lines {
			// if len element > min : adjust it
			n := len(line[i])
			if n > min {
				headerSizes[i] = n
			}
		}
		// apply coef to text length to get lenght in pixels
		headerSizes[i] = headerSizes[i] * coef
	}
	return headerSizes
}

//
// other tools
//

// GuessSeparator : try to guess cvs separator ( ; or , )
func GuessCvsSeparator(line string) (sep rune) {

	return sep
}
