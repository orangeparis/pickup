package tools

/*


hellpers to manipulate csv headers

*/

var defaultSizeCoeff = 10

// ComputeSizeHeaders : compute headers size in pixel for display
func ComputeSizeHeaders(headers []string, lines [][]string) (headerSizes []int) {

	// coef for ratio text lenght -> pixel
	coef := defaultSizeCoeff

	for i, header := range headers {
		// minimum size of header is len of the label
		min := len(header)
		headerSizes = append(headerSizes, min)
		for _, line := range lines {
			// if len element > min : adjust it
			n := len(line[i])
			if n > min {
				headerSizes[i] = n
			}
		}
		// apply coef to text length to get lenght in pixels
		headerSizes[i] = headerSizes[i] * coef
	}
	return headerSizes
}

// AddLeftColumn  insert columnon the left at the start
func AddLeftColumn(headers []string, lines [][]string, label string, content string) (heads []string, contents [][]string) {

	if label == "" {
		label = "Select"
	}
	heads = append(heads, label)
	heads = append(heads, headers...)
	for _, line := range lines {
		var newLine []string
		newLine = append(newLine, content)
		newLine = append(newLine, line...)
		contents = append(contents, newLine)
	}
	return heads, contents
}

// DeleteLeftColumn  remove column the left
func DeleteLeftColumn(headers []string, lines [][]string) (heads []string, contents [][]string) {

	if len(headers) >= 1 {
		heads = headers[1:]
	}
	for _, line := range lines {
		if len(line) >= 1 {
			newLine := line[1:]
			contents = append(contents, newLine)
		}
	}
	return heads, contents
}
