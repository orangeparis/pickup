package tools_test

import (
	"testing"

	"bitbucket.org/orangeparis/pickup/tools"
)

func TestComputeSizeheaders(t *testing.T) {

	headers := []string{"my", "Three", "Columns"}
	lines := [][]string{}
	lines = append(lines, []string{"first", "line", "content,and long content"})
	sizes := tools.ComputeSizeHeaders(headers, lines)
	if sizes[2] != 240 {
		t.Fail()
	}

}

func TestAddLeftColumn(t *testing.T) {

	headers := []string{"my", "Three", "Columns"}
	lines := [][]string{}
	lines = append(lines, []string{"first", "line", "content,and long content"})
	lines = append(lines, []string{"second", "line", "content2"})

	label := "Select"
	content := "0"
	heads, contents := tools.AddLeftColumn(headers, lines, label, content)

	if len(heads) != 4 {
		t.Fail()
		return
	}
	if heads[0] != "Select" {
		t.Fail()
		return
	}
	for _, line := range contents {
		if len(line) != 4 {
			t.Fail()
			return
		}
		if line[0] != content {
			t.Fail()
			return
		}
	}

}

func TestDeleteLeftColumn(t *testing.T) {

	headers := []string{"Select", "my", "Three", "Columns"}
	lines := [][]string{}
	lines = append(lines, []string{"0", "first", "line", "content,and long content"})
	lines = append(lines, []string{"1", "second", "line", "content2"})

	heads, contents := tools.DeleteLeftColumn(headers, lines)

	if len(heads) != 3 {
		t.Fail()
		return
	}
	if heads[0] != "my" {
		t.Fail()
		return
	}
	for _, line := range contents {
		if len(line) != 3 {
			t.Fail()
			return
		}
	}

}
