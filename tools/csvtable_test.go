package tools_test

import (
	"io/ioutil"
	"log"
	"testing"

	"bitbucket.org/orangeparis/pickup/tools"
)

var vmfilename = "../mock/deployvm.csv"

func aSampleCsvTable() *tools.CsvTable {

	tbl := &tools.CsvTable{}

	tbl.Headers = []string{"my", "Three", "Columns"}
	tbl.Lines = append(tbl.Lines, []string{"first", "line", "content,and long content"})
	tbl.Lines = append(tbl.Lines, []string{"second", "line", "content"})
	return tbl
}

func TestCreateCsvTable(t *testing.T) {

	headers := []string{"my", "Three", "Columns"}
	lines := [][]string{}
	lines = append(lines, []string{"first", "line", "content,and long content"})

	tbl := tools.NewCsvTable(headers, lines)

	if len(tbl.Headers) != 3 {
		t.Fail()
	}
	if len(tbl.Lines) != 1 {
		t.Fail()
	}
}

func TestTableLoadFromCsv(t *testing.T) {

	data, err := ioutil.ReadFile(vmfilename)
	if err != nil {
		log.Fatal(err)
	}

	tbl := &tools.CsvTable{}

	err = tbl.LoadFromCsv(data, ';')

	if len(tbl.Headers) != 52 {
		t.Fail()
	}
	if len(tbl.Lines) != 21 {
		t.Fail()
	}

}

func TestTableHeaderSize(t *testing.T) {

	tbl := aSampleCsvTable()

	sizes := tbl.SizeHeaders(10)
	if sizes[2] != 240 {
		t.Fail()
	}

}

func TestTableAddLeftColumn(t *testing.T) {

	tbl := aSampleCsvTable()

	label := "Select"
	content := "0"

	tbl.AddLeftColumn(label, content)
	if len(tbl.Headers) != 4 {
		t.Fail()
		return
	}
	if tbl.Headers[0] != label {
		t.Fail()
		return
	}
	for _, line := range tbl.Lines {
		if len(line) != 4 {
			t.Fail()
			return
		}
		if line[0] != content {
			t.Fail()
			return
		}
	}

}

func TestTableDeleteLeftColumn(t *testing.T) {

	tbl := aSampleCsvTable()

	tbl.DeleteLeftColumn()

	if len(tbl.Headers) != 2 {
		t.Fail()
		return
	}
	if tbl.Headers[0] != "Three" {
		t.Fail()
		return
	}
	for _, line := range tbl.Lines {
		if len(line) != 2 {
			t.Fail()
			return
		}
	}

}
